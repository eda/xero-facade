# Xero-facade npm Documentation
For usage with EDA admissions prototype

**IMPORTANT FOR NPM TO WORK**
This Npm uses [dotenv](https://www.npmjs.com/package/dotenv) and [fs](https://nodejs.org/api/fs.html) for handling credentials for xero authentication.

Xero utilises a consumer key, consumer secret and a private key .pem file for authentication - [Documentation](http://developer.xero.com/documentation/advanced-docs/public-private-keypair/) for instructions
Create a .env file and set the following:
* CONSUMER_KEY - 'your consumer key'
* CONSUMER_SECRET - 'your consumer secret'

Put the .pem file in the root directory.
Initializing Xero is done below:

```
var xeroCredentials = {
  consumerKey: process.env.CONSUMER_KEY,
  consumerSecret: process.env.CONSUMER_SECRET,
  privateKey: fs.readFileSync('path to .pem file')
};
var xero = require('@eda/xero-facade')(xeroCredentials);
```


## createAnInvoice(invoiceInfo) function -
Creates a new invoice in Xero marked as DRAFT

```
xero.createAnInvoice(invoiceInfo)

```

Expects a student object with the following structure:

```
var invoiceInfo = {
  contactName: 'Example Name',
  contactId: 'Xero contactId',
  date:{
    P0: phase Zero date,
    BC: bootcamp date,
    Grad: graduation date
  }
};
```

AccountCode - Must match Xero account codes otherwise will default to invoice code
Dates - Must match following format - Year/month/Day - '2015-02-23'

Console logs the response message from Xero


## fetchContacts() function -
Returns all contacts in Xero


##createContact(contacInfo) function
###Creates a new contact in xero
A new contact in xero is created when a student object is passed in that matches the structure below - all fields must be entered
```
 {
      FirstName: example First Name,
      LastName: example Last Name,
      EmailAddress: example Email Address,
      AccountNumber: example relate iq Id,
      ContactNumber: example relate iq Id
}

```
