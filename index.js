var Xero = require('xero')
var xero

var initialize = function initialize (credentials) {
  var CONSUMER_KEY = credentials.consumerKey
  var CONSUMER_SECRET = credentials.consumerSecret
  var PRIVATE_KEY = credentials.privateKey
  xero = new Xero(
    CONSUMER_KEY,
    CONSUMER_SECRET,
    PRIVATE_KEY
  )

  return {
    fetchContacts: fetchContacts,
    createAnInvoice: createAnInvoice,
    createContact: createContact,
    getTrackingCodes: getTrackingCodes
  }
}

var createContact = function createContact(contactInfo,failure,success) {
  var request = {
    Name: contactInfo.name,
    FirstName: contactInfo.firstName,
    // LastName: contactInfo.lastName,
    EmailAddress: contactInfo.email,
    AccountNumber: contactInfo.relateId,
    ContactNumber: contactInfo.relateId,
    SkypeUserName: "schedule=bootcamp"
  }
  xero.call('POST', '/contacts', request, function(err,json) {
    if(err) {
      failure(err)
    } else {
      success(json)
    }
  })
}

var fetchContacts = function fetchContacts (failure, success) {
  xero.call('GET', '/contacts', null, function (err, json) {
    if (err) {
      failure(err)
    } else {
      success(json)
    }
  })
}

var getTrackingCodes = function getTrackingCodes(failure, success){
  // tracking codes for cohort and for business unit
  xero.call('GET','/TrackingCategories',null, function(err,json){
    if(err){
      failure(err)
    }else{
      success(json)
    }
  })
}

var createAnInvoice = function createAnInvoice (invoiceInfo,failure,success) {
  var currentDate = new Date()
  // add in
  var depositDefaults = {
    itemCode: 'dep',
    type: 'Deposit',
    amount: '1000.00',
    accountCode:'210'
  }
  var feeDefaults = {
    itemCode:'fee',
    type:'Fee',
    amount: '10000.00',
    accountCode:'210'
  }
  var trackingDefault = {
    'TrackingCategory':{
      'TrackingCategoryID':'3d8de69a-8aea-4c55-8cd6-daaf1503e684',
      'Name':'Product',
      'Option':'Bootcamp'
    }
  }
  var request = {
    'Type': 'ACCREC',
    'Contact': {
      'ContactID': invoiceInfo.contactId,
      'Name': invoiceInfo.name
    },
    'Date': currentDate,
    // 'DueDate':"",
    'LineAmountTypes': 'Inclusive',
    'Reference':'Student Fees',
    'LineItems':[
        {
          'ItemCode': depositDefaults.itemCode,
          'Description': depositDefaults.type,
          'Quantity': 1,
          'UnitAmount':depositDefaults.amount,
          'AccountCode':depositDefaults.accountCode,
          'Tracking':trackingDefault
        },
        {
          'ItemCode': feeDefaults.itemCode,
          'Description': feeDefaults.type,
          'Quantity': 1,
          'UnitAmount':feeDefaults.amount,
          'AccountCode':feeDefaults.accountCode,
          'Tracking':trackingDefault
        }
      ],
    'Status': 'DRAFT',
  }

  var setDueDate = function(){
    calculateInvoiceDueDate(invoiceInfo,currentDate,function(result){
      request.DueDate = result
    })
  }

  xero.call('POST', '/invoices?SummarizeErrors=false', request, function (err, json) {
    console.log(request)
    if (err) {
      failure(err)
    } else {
      success(json)
    }
  })
}

function calculateInvoiceDueDate(invoiceInfo,currentDate,callback){
  // have to simulate this till the date app

  if(currentDate < invoiceInfo.date.phaseZero){
    callback(invoiceInfo.date.phaseZero)
  }else
    _calculateDueDate(currentDate, function(returnedDate){
      callback(returnedDate)
    })
}

function _calculateDueDate(currentDate,callback){
  var currentDay = currentDate.getDate()
  var currentMonth = currentDate.getMonth()+1
  var currentYear = currentDate.getFullYear()
  var daysInMonth = new Date(currentYear,currentMonth,0).getDate()
  if((currentDay + 7) < daysInMonth ){
    callback(currentYear+'-'+currentMonth+'-'+(currentDay + 7))
  }else {
    var remainder = (7-(daysInMonth - currentDay))
    if(currentMonth === 12){
      callback((currentYear+1)+'-01-' +remainder)
    }else{
      callback((currentYear)+'-'+(currentMonth+1)+'-' +remainder)
    }
  }
}

module.exports = initialize
